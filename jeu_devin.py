from random import randint
def user_devin():
    '''
    R0 : La machine fait deviner un nombre a l'utilisateur.
    '''
    # Choisir un nombre aleatoire
    cible = randint(1,10000)
    print('J\'ai choisi un nombre entre 1 et 9999.')
    print('A vous de le deviner')
    
    # Faire deviner ce nombre à l'utilisateur
    trouve = False
    nb_essais = 0

    while not trouve :
        # Recueillir la tentative de l'utilisateur
        guess = int(input('Tentez votre chance: ')) 
        #Afficher l'indice approprié à la tentative de l'utilisateur
        if (guess > cible) : 
            print('Plus petit')
        elif (guess < cible) :
            print('Plus grand')
        else :
            print('Trouvé')
            trouve = True
        nb_essais += 1

    # Afficher le nombre d'essais
    print(f'Vous avez trouvé en {nb_essais} essais. Bravo.')

def machine_devin():
    '''
    R0 : L'utilisateur fait deviner un nombre à la machine
    '''
    #Expliquer les régles
    print('Choisissez un nombre entre 1 et 9999, je vais le deviner')
    #Demander à l'utilisateur si il est prêt
    pret = False
    while not pret:
        reponse = input('Etes vous prêt ? o/n ').lower()
        pret = (reponse == 'o')
    trouve = False
    #Conserver le nombre d'essais
    nb_essais = 0 
    #Bornes de dichotomie
    a = 0 
    b = 10000
    while not trouve:
        nb_essais += 1
        milieu = (b+a)//2
        print(f'Proposition {nb_essais} : {milieu}')
        #Recueillir l'indice de l'utilisateur
        mauvais_indice = True
        while  mauvais_indice :
            indice = input('Trop (g)rand(>), trop (p)etit(<) ou (t)rouvé(=) ? ').lower()
            #Affiner la recherche grâce à l'indice de l'utilisateur
            if indice == 'g' or indice == '>':
                b = milieu
                mauvais_indice = False
            elif indice == 'p' or indice == '<':
                a = milieu
                mauvais_indice = False
            elif indice == 't' or indice == '=':
                trouve = True
                mauvais_indice = False
            else :
                print('Je n\'ai pas compris la réponse. Merci de répondre : ')
                print('g ou > si ma proposition est trop grande')
                print('p ou < si ma proposition est trop petite')
                print('t ou = si j\'ai trouvé le nombre')
    print(f'J\'ai trouvé en {nb_essais} essais')

def jeu_devin():
    '''
    R0 : Proposer les options et rediriger vers la version du jeu voulue
    '''
    choix = 'choix'
    #Menu de sélection du mode de jeu.
    while choix != '0':
        print('Que voulez vous faire ?')
        print('1- L\'ordinateur choisit un nombre et vous le devinez')
        print('2- Vous choisissez un nombre et l\'ordinateur devine')
        print('0- Quitter le programme')
        choix = input('Votre choix : ')
        if choix == '1':
            user_devin()
        elif choix == '2':
            machine_devin()
        elif choix == '0':
            print( 'Au revoir')
        


jeu_devin()
